#!/usr/bin/env python
""" Show diagnostic info from rostest2gradescope.py """

import rostest2gradescope as r2g
r2g.diagnostic()
