# Make workspace
mkdir -p "${ROS_WS_DIR}/src"
if test "${MODE}" == 'rostest'
then
    mkdir -p "${ROSTEST_RESULTS_DIR}"

    # Clone autograder files (public repository)
    if test ! -d "${ROS_AUTOGRADER_DIR}"
    then
        git clone https://tronroberto@bitbucket.org/tronroberto/ros_autograder.git "${ROS_AUTOGRADER_DIR}"
    fi
fi

# Clone me416_lab repository (student version)
if test ! -d "${ROS_WS_DIR}/src/me416_lab"
then
    git clone https://tronroberto@bitbucket.org/tronroberto/ros2me416.git "${ROS_WS_DIR}/src/me416_lab"
fi
if test ! -d "${ROS_WS_DIR}/src/me416_msgs"
then
    git clone https://tronroberto@bitbucket.org/tronroberto/ros2me416_msgs.git "${ROS_WS_DIR}/src/me416_msgs"
fi

# Build the workspace
if test "${LOCATION}" != "macOS"
then
    cd "${ROS_WS_DIR}"
    colcon build
    source install/setup.bash
fi

# Enable ROS configuration for every shell
# This also changes the Python path to find the Python packages in the workspace
if test "${LOCATION}" == "remote"
then
    echo "source /opt/ros/${ROS_DISTRO}/setup.bash" >> /etc/bash.bashrc
    echo "source ${ROS_WS_DIR}/install/setup.bash" >> /etc/bash.bashrc
fi
