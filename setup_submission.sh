# Copy submission files to destination directory
if test "${MODE}" == "pytest"
then
    SUBMISSION_DEST_DIR="${SOURCE_DIR}"
else
    SUBMISSION_DEST_DIR="${NODES_DIR}"
    #cp -v "${ROS_WS_DIR}/src/me416_lab/nodes/me416_utilities.py" "${SUBMISSION_DEST_DIR}"
    #cp -v "${ROS_WS_DIR}/src/me416_lab/nodes/scripted_op.csv" "${SUBMISSION_DEST_DIR}"
fi
cp -v "${SUBMISSION_DIR}"/*.py "${SUBMISSION_DEST_DIR}"
chmod u+x "${SUBMISSION_DEST_DIR}"/*.py
dos2unix "${SUBMISSION_DEST_DIR}"/*.py

# Move files (if present) to the correct modules
for MODULE in robot_model controller
do
    FILE_SOURCE="${SUBMISSION_DIR}/${MODULE}.py"
    FILE_DEST="${ROS_WS_DIR}/src/me416_lab/${MODULE}/${MODULE}.py"
    echo "Checking if ${FILE_SOURCE} is present..."
    if test -f ${FILE_SOURCE}
    then
        echo "Copying to ${FILE_DEST}"
        cp -v ${FILE_SOURCE} ${FILE_DEST}
    else
        echo "Not present"
    fi
done
