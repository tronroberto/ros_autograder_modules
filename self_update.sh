#!/usr/bin/env bash
set -x #echo every command
set -e #exit on error

# location and root dir setup
USER=`whoami`
if test "$USER" == "root"
then
    LOCATION="remote"
    AUTOGRADER_DIR="/autograder"
else
    LOCATION="local"
    AUTOGRADER_DIR="$HOME/gradescope_root/autograder"
fi
SOURCE_DIR="${AUTOGRADER_DIR}/source"

# Setup variables. Note that if we are on Gradescope, the files in SOURCE_DIR will have already been copied,
# but if we are local we might be setting up from another directory
if test "${LOCATION}" == "remote"
then
    CWD="${SOURCE_DIR}"
else
    CWD=`pwd`
fi
source setup_variables.sh
source setup_selfupdate.sh
#Update tests
source setup_tests.sh
