#!/usr/bin/env bash
set -x #echo every command
set -e #stop script on error

# location and root dir setup
USER=`whoami`
if test "$USER" == "root"
then
    LOCATION="remote"
    AUTOGRADER_DIR="/autograder"
else
    SYSTEM=`uname`
    if [ "${SYSTEM}" == "Darwin" ]
    then
       LOCATION="macOS"
       CWD=`pwd`
       DIRNAME=$(basename ${CWD})
       if [ "${DIRNAME}" == "autograder" ]
       then
           echo "Setting up in a test environment"
           AUTOGRADER_DIR="${CWD}"
       else
           echo "Setting up from a clone of the repo"
           AUTOGRADER_DIR="${CWD}/gradescope_root/autograder"
       fi
    else
        LOCATION="local"
        AUTOGRADER_DIR="$HOME/gradescope_root/autograder"
    fi
fi
SOURCE_DIR="${AUTOGRADER_DIR}/source"

# Setup variables. Note that if we are on Gradescope, the files in SOURCE_DIR will have already been copied,
# but if we are local we might be setting up from another directory
if test "${LOCATION}" == "remote"
then
    CWD="${SOURCE_DIR}"
else
    CWD=`pwd`
fi
source "${CWD}/setup_variables.sh"

if test "$LOCATION" == "remote"
then
    cd "${SOURCE_DIR}"
    source setup_git.sh
    cd "${SOURCE_DIR}"
    source setup_ros.sh

    cd "${SOURCE_DIR}"
    source setup_python.sh
else # "$LOCATION" == "local" or "$LOCATION" == "macOS"
    if test ! -d "${SOURCE_DIR}"
    then
        if test "$LOCATION" == "macOS"
        then
            AUTOGRADER_SOURCE_URL="/Users/tron/BU/Teaching/ME416-Robotics/assignments/autograders/autograder_withROS"
        else
            AUTOGRADER_SOURCE_URL="https://tronroberto@bitbucket.org/tronroberto/ros_autograder_modules.git"
        fi
        git clone "${AUTOGRADER_SOURCE_URL}" "${SOURCE_DIR}"
    fi
    cp "${CWD}/setup_settings" "${SOURCE_DIR}"
    cp "${SOURCE_DIR}/run_autograder" "${SOURCE_DIR}/.."
fi
# The line below is useful during debug of the setup scripts,
# otherwise it can be just ignored
# source setup_selfupdate

mkdir -p "${AUTOGRADER_DIR}/results"

cd "${SOURCE_DIR}"
source setup_workspace.sh
cd "${SOURCE_DIR}"
source setup_tests.sh

if "${LOCATION}" == "macOS"
then
    cp setup_settings "${AUTOGRADER_DIR}"
fi
