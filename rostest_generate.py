#!/usr/bin/env python
""" Generate a bash script to run all rostests """

import rostest2gradescope as r2g

r2g.test_list_to_file('rostest_run.sh')
