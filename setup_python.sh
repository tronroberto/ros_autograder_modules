# This file is meant to be sourced by source.sh

# Install pip + requirements
apt-get install -y python3 ipython3 python3-pip python3-dev python3-ipdb

pip install -r "${SOURCE_DIR}/requirements.txt"
