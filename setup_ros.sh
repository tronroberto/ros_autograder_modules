# This file is meant to be sourced by source.sh

# Source: https://docs.ros.org/en/humble/Installation/Ubuntu-Install-Debians.html

set -e

ROS_DISTRO="humble"

# Check that the locale is correct
if ! test $LANG==en_US.UTF-8
then
    echo "Incorrect locale, see https://docs.ros.org/en/${ROS_DISTRO}/Installation/Ubuntu-Install-Debians.html"
    exit 1
fi

# Ensure that the Ubuntu Universe repository is enabled
apt install -y software-properties-common
add-apt-repository -y universe

# Add the ROS 2 GPG key, and then the repository to the source list
 curl -sSL https://raw.githubusercontent.com/ros/rosdistro/master/ros.key -o /usr/share/keyrings/ros-archive-keyring.gpg
echo "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/ros-archive-keyring.gpg] http://packages.ros.org/ros2/ubuntu $(. /etc/os-release && echo $UBUNTU_CODENAME) main" |  tee /etc/apt/sources.list.d/ros2.list > /dev/null

# Update package cache and upgrade outdated packages
apt update
apt upgrade -y

# Install ROS
apt install -y ros-${ROS_DISTRO}-ros-base "ros-${ROS_DISTRO}-rqt-*" ros-${ROS_DISTRO}-v4l2-camera python3-colcon-common-extensions


# Automatically source underlay
source /opt/ros/${ROS_DISTRO}/setup.bash
if ! grep "${ROS_DISTRO}" ~/.bashrc
then
    echo "source /opt/ros/${ROS_DISTRO}/setup.bash" >> ~/.bashrc
    echo "export ROS_LOCALHOST_ONLY=1" >> ~/.bashrc
fi
