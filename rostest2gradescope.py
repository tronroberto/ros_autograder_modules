#!/usr/bin/env python
""" Run rostest, format output as JSON for Gradescope """
from __future__ import print_function

import json
import os
import re
import subprocess
import sys
import xmltodict


def eprint(*args, **kwargs):
    """
    Print to stderr
    """
    print(*args, file=sys.stderr, **kwargs)


SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))

if 'ROSTEST_RESULTS_DIR' in os.environ:
    RESULTS_DIR = os.path.join(os.environ['ROSTEST_RESULTS_DIR'])
else:
    RESULTS_DIR = os.path.join(SCRIPT_DIR, 'test_results', 'ros_autograder')

if 'TESTS_DIR' in os.environ:
    TESTS_DIR = os.path.join(os.environ['TESTS_DIR'], 'tests')
else:
    TESTS_DIR = os.path.join(SCRIPT_DIR, 'tests')

if 'SOURCE_DIR' in os.environ:
    SOURCE_DIR = os.environ['SOURCE_DIR']
else:
    SOURCE_DIR = SCRIPT_DIR


def diagnostic():
    print('Value of os.environ with key containing ROS or DIR')
    items = os.environ.items()
    items.sort()
    for kv in items:
        if re.search('(ROS|DIR)', kv[0]) is not None:
            print(' %s = %s' % kv)
    print('TESTS_DIR =', TESTS_DIR)
    print('RESULTS_DIR =', RESULTS_DIR)
    print('Contents of TESTS_DIR')
    try:
        print(os.listdir(TESTS_DIR))
    except:
        print('Failed')
    print('Parsed results.json')
    results_json = load_results()
    print(json.dumps(results_json))


def normalize_xml_text(s):
    s = s.replace('<![CDATA[', '')
    s = s.replace(']]>', '')
    s = re.sub(r'&#x001B;\[\d+m', '', s)
    return s


def load_xml_results(filename):
    d = {}
    fullfile = os.path.join(RESULTS_DIR, filename)
    with open(fullfile, 'r') as f_in:
        s = f_in.read()
        d = xmltodict.parse(s)
        d = d['testsuite']

    return d


def default_description(xml_filename):
    descriptions = {
        'hz': 'Check publishing rate',
        'publish': 'Check that a topic is published at least once'
    }
    for testname, description in descriptions.items():
        if xml_filename == 'rosunit-' + testname + '.xml':
            return description + '\n'
    return ''


def xml2json_units(d_suite, suitename=None):
    '''
    Load the results from a test unit xml file into a dictionary
    '''
    test_names = [re.sub('^test', '', x['@name']) for x in d_suite['testcase']]
    file_name_prefix = 'rosunit-'
    if suitename:
        suitename_short = re.sub('(rostest-|tests_|.xml)', '', suitename)
        file_name_prefix = suitename_short + '-' + file_name_prefix
    file_names = [file_name_prefix + x + '.xml' for x in test_names]
    tests = []
    for xml_filename in file_names:
        d_unit = load_xml_results(xml_filename)
        max_score = int(d_unit['@tests'])
        score = max_score - int(d_unit['@failures']) - int(d_unit['@errors'])
        unitname = xml_filename
        unitname = re.sub('(\.xml|rostest-|rosunit-)', '', unitname)
        #print(unitname)
        d_unit_out = {
            'name': unitname,
            'score': score,
            'max_score': max_score,
            'output': ''
        }

        # add description to output
        description = default_description(xml_filename)
        if description:
            d_unit_out['output'] += 'Description: ' + description

        # add result status to output
        d_unit_out['output'] += 'Result: '
        if score == max_score:
            d_unit_out['output'] += 'Passed\n'
        else:
            d_unit_out['output'] += 'Failed\n'
            d_unit_out['output'] += normalize_xml_text(
                d_unit['system-out'] + d_unit['system-err']) + '\n'
            if 'failure' in d_unit['testcase']:
                d_unit_out['output'] += d_unit['testcase']['failure'][
                    '@type'] + ': ' + d_unit['testcase']['failure']['#text']

        # we are done with this test unit
        tests.append(d_unit_out)
    return tests


def xml2json_suite(d_suite, output_header=None):
    output = normalize_xml_text(d_suite['system-err'])
    if output_header:
        output = '-- ' + d_suite['@name'] + ' ' + '-' * 10 + '\n' + output

    d_out = {
        'execution_time':
        float(d_suite['@time']),
        'output':
        output,
        'stdout_visiblity':
        'visible',
        'score':
        int(d_suite['@tests']) - int(d_suite['@failures']) -
        int(d_suite['@errors']),
        'tests':
        xml2json_units(d_suite, output_header)
    }
    return d_out


def xml2json(filename):
    d_suite = load_xml_results(filename)
    d_out = xml2json_suite(d_suite, filename)
    return d_out


def json_merge(d_all, d_out):
    for field in ['execution_time', 'output', 'tests']:
        d_all[field] += d_out[field]
    return d_all


def test_list_find():
    """ Find list of all rostests by looking for the files """
    test_list = [x for x in os.listdir(TESTS_DIR) if '.test' in x]
    return test_list


def test_list_cmd(test_list=None):
    """ Generate array rostest command line commands given a test list """
    if test_list is None:
        test_list = test_list_find()
    cmd_list = [['rostest', 'ros_autograder', test_name]
                for test_name in test_list]
    return cmd_list


def test_list_to_file(file_name):
    """ Write a Bash script with all the rostest command line commands """
    test_list = test_list_find()
    cmd_list = test_list_cmd(test_list)
    test_names = [f.replace('.test', '') for f in test_list]
    file_name = os.path.join(SOURCE_DIR, file_name)
    with open(file_name, 'w') as outfile:
        outfile.write('#!/usr/bin/env bash\n')
        outfile.write('set -x # echo every command')
        outfile.write('pushd .\n')
        outfile.write('cd ' + RESULTS_DIR + '\n')
        for cmd, test_name in zip(cmd_list, test_names):
            outfile.write(' '.join(cmd) + '\n')
            outfile.write('echo Renaming rostest unit xml files for ' +
                          test_name + '\n')
            outfile.write('for unit in rosunit-*.xml\n')
            outfile.write('do\n')
            outfile.write('mv -v $unit ' + test_name + '-$unit\n')
            outfile.write('done\n')
        outfile.write('popd\n')
        outfile.write('true # To make sure this script returns true\n')


def results_collect(test_list=None):
    """
    Collect results of rostests, and create a Gradescope-compatible JSON
    data structure
    """
    if test_list is None:
        test_list = test_list_find()

    d_all = None
    for testname in test_list:
        d_out = xml2json('rostest-tests_' + testname.replace('.test', '.xml'))
        if not d_all:
            d_all = d_out
        else:
            d_all = json_merge(d_all, d_out)

    # calculation of final score
    d_all['score'] = sum([a['score'] for a in d_all['tests']])
    return d_all


def load_results(filename=os.path.join(SOURCE_DIR, '..', 'results',
                                       'results.json')):
    """
    Load the Gradescope results written in the result.json file
    """
    data = {}
    with open(filename, 'rt') as fid:
        data = json.load(fid)
    return data


def runner():
    # Find .test files
    test_list = test_list_find()

    d_all = None
    eprint(os.environ)
    for testname in test_list:
        # TODO: do not hardcode script's path
        cmd = [
            '/autograder/ws/src/ros_autograder/scripts/rostest_with_source.sh',
            testname
        ]  # ,'>',testname+'.out']
        # TODO: figure out how to avoid a subprocess call by importing rostest
        with open(testname + '.out', 'w') as outfile:
            subprocess.call(cmd, shell=True)
            # subprocess.call(cmd, stdout=outfile, shell=True)
        #    p = subprocess.Popen(cmd, stdout=outfile)
        #    os.waitpid(p.pid, 0)
        d_out = xml2json('rostest-tests_' + testname.replace('.test', '.xml'))
        if not d_all:
            d_all = d_out
        else:
            d_all = json_merge(d_all, d_out)
    print(json.dumps(d_all))
    return d_all


if __name__ == '__main__':
    runner()
