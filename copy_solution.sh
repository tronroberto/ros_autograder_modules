#!/usr/bin/env bash
set -x
set -e

LOCATION="remote"
AUTOGRADER_DIR="$HOME/gradescope_root/autograder"
SOURCE_DIR="${AUTOGRADER_DIR}/source"
CWD="${SOURCE_DIR}"
source setup_variables.sh

mkdir -p ${SUBMISSION_DIR}
TESTS_NAME=$(basename ${TESTS_SOURCE_DIR})

cp -vr ~/autograders/autograder_solutions/${TESTS_NAME}/* ${SUBMISSION_DIR}
