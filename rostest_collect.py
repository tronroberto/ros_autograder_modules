#!/usr/bin/env python
""" Collect results from rostests and output JSON to console """

import json
import rostest2gradescope as r2g

result_data=r2g.results_collect()
print(json.dumps(result_data))