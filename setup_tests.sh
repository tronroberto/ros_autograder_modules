# Clone or update actual tests (requires deploy key) and copy them
if test ! -d "${TESTS_REPO_DIR}"
then
    git clone $GIT_REPO "${TESTS_REPO_DIR}"
else
    if test ${LOCATION} == "remote"
    then
        cd "${TESTS_REPO_DIR}"
        git pull
    fi
fi

if [ "$MODE" == "pytest" ]
then
    echo Check on test directory for pytest
    if test -d "${TESTS_DIR}"
    then
        rm -rf "${TESTS_DIR}"
    fi
    mkdir "${TESTS_DIR}"
else
    echo Check on test directory for rostest
    if test -d "${TESTS_DIR}/tests"
    then
        rm -rf "${TESTS_DIR}/tests"
    fi

    if test -d "${TESTS_DIR}/tests_fixtures"
    then
        rm -rf "${TESTS_DIR}/tests_fixtures"
    fi
fi

cp -vr "${TESTS_SOURCE_DIR}"/* "${TESTS_DIR}"
