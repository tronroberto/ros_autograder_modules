if test ! -d /tmp/ros_autograder_modules
then
    git clone git@bitbucket.org:tronroberto/ros_autograder_modules.git /tmp/ros_autograder_modules
fi
cd /tmp/ros_autograder_modules
git pull
cp -f /tmp/ros_autograder_modules/* "${SOURCE_DIR}"
cp "${SOURCE_DIR}/run_autograder" "${AUTOGRADER_DIR}"
