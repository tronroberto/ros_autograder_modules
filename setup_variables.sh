#This file is meant to be sourced by setup.sh and run_autograder

#Variables to change to match your setup
GIT_HOST="bitbucket.org" #change to github.com if using GitHub
GIT_REPO="git@bitbucket.org:tronroberto/rosme416autograders.git"

#Variables for the setup and run scripts
SSH_DIR="$HOME/.ssh"

# Workspace
ROS_WS_DIR="$AUTOGRADER_DIR/ws"

# Autograder repo
ROS_AUTOGRADER_DIR="$ROS_WS_DIR/src/ros_autograder"

# Tests repo
if test ${LOCATION} == "local"
then
    TESTS_REPO_DIR="${HOME}/autograders/autograder_tests"
else
    TESTS_REPO_DIR="$AUTOGRADER_DIR/tests_repository"
fi

source "${CWD}/setup_settings"

# Set up autograder files
SUBMISSION_DIR="${AUTOGRADER_DIR}/submission"
RESULT_DIR="${AUTOGRADER_DIR}/results"
SOURCE_DIR="${AUTOGRADER_DIR}/source"

if [ "$MODE" == "pytest" ]
then
    TESTS_DIR="${SOURCE_DIR}/tests"
else
    TESTS_DIR="${ROS_AUTOGRADER_DIR}"
    NODES_DIR="${ROS_AUTOGRADER_DIR}/nodes"
    # Where rostest will put the results
    ROSTEST_RESULTS_DIR="$HOME/.ros/test_results/ros_autograder"

    # Some environmental variables will be used by the Python script, so export them
    export ROSTEST_RESULTS_DIR
    export TESTS_DIR
fi

export SOURCE_DIR
