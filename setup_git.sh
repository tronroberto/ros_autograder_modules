#This file is meant to be sourced by setup.sh

source setup_ssh.sh

# Set Git identity (placeholder)
git config --global user.email "autograder@placeholder.org"
git config --global user.name "Gradescope Autograder"
